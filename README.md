# NOAA API Client (F# (.NET Standard 2.0))

This repository contains REST Web Client, targeting 
[NOAA's Climate Data Webservice](https://www.ncdc.noaa.gov/cdo-web/webservices/v2#gettingStarted).
There is no intended use case for this package, but the goal is to fully implement
a package that can utilize the full extent of the web service, in F#, targeting
.NET Standard 2.0, so that it is possible to use the project in both .NET Core applications (2.0 and above)
and .NET Framework applications (4.6.2 and above).


