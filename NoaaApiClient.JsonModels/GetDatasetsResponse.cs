using System;
using System.Collections.Generic;
using RestSharp;
using RestSharp.Deserializers;

namespace NoaaApiClient.JsonModels
{
    public class GetDatasetsResponse
    {
            [DeserializeAs(Name = "results")]
            public List<DatasetJsonModel> Results { get; set; }

            [DeserializeAs(Name = "metadata")]
            public Dictionary<string, Dictionary<string, object>> Metadata { get; set; }
    }
}
