﻿using System;
using RestSharp;
using RestSharp.Deserializers;

namespace NoaaApiClient.JsonModels
{
    public class DatasetJsonModel
    {
            [DeserializeAs(Name = "id")]
            public string Id { get; set; }

            [DeserializeAs(Name = "uid")]
            public string Uid { get; set; }
            
            [DeserializeAs(Name = "mindate")]
            public DateTime MinDate { get; set; }
            
            [DeserializeAs(Name = "maxdate")]
            public DateTime MaxDate { get; set; }
            
            [DeserializeAs(Name = "name")]
            public string Name { get; set; }

            [DeserializeAs(Name = "datacoverage")]
            public string DataCoverage { get; set; }
    }
}
