namespace NoaaApiClient.V2

open System
open RestSharp
open NoaaApiClient.V2.Enumerations
open NoaaApiClient.V2.Endpoints

/// <summary>
/// Initializes a <see cref="RestSharp.RestClient">RestSharp.RestClient</see>
/// with the base url and the token set in the header.
/// </summary>
/// <param name="baseurl">Specifies the base URL for all API calls.</param>
/// <param name="token">Specifies the access token, required for using the API.</param>
type NoaaApiClient (baseurl: string, token: string) =
    let _client = new RestClient(baseurl)
    do _client.AddDefaultHeader("token", token)

    /// <summary>
    /// Returns a new instance of a <see cref="Datasets.Client">Datasets.Client</see>
    /// object, the sub-client that wraps the "/datasets" endpoint of the
    /// NOAA Data API.
    /// </summary>
    member this.Datasets = (new Datasets.Client (_client))
