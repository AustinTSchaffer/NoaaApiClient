namespace NoaaApiClient.V2.Enumerations

type SortOrder = 
    | ASCENDING
    | DESCENDING

/// <summary>
/// Defines functions for converting enumeration types to values that are
/// used by the NOAA API.
/// </summary>
module EnumerationConversions =

    /// <summary>
    /// Converts a <see cref="SortOrder">SortOrder</see> to a string.
    /// </summary>
    let sortOrderToString = function
        | ASCENDING -> "asc"
        | DESCENDING -> "desc"
