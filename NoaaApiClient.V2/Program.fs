namespace NoaaApiClient.V2

module Program =

    open System
    open NoaaApiClient.V2
    open NoaaApiClient.V2.Endpoints

    [<EntryPoint>]
    let main _ =
        let c = NoaaApiClient (NoaaApiClient.V2.Constants.BASEURL, "<redacted>")
        let dsresponse = c.Datasets.GetDatasets Datasets.DefaultGetOptions
        printf "%A" dsresponse.ResponseStatus
        0
