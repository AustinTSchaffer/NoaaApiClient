namespace NoaaApiClient.V2.Endpoints

open System
open RestSharp
open NoaaApiClient.JsonModels
open NoaaApiClient.V2.Constants
open NoaaApiClient.V2.Endpoints
open NoaaApiClient.V2.Enumerations

/// <summary>
/// Defines types, values, and functions that can be used 
/// to interact with the `"/datasets"` endpoint of the NOAA
/// API.
/// </summary>
module Datasets =

    /// <summary>
    /// Defines the configuration options exposed by the
    /// GetDatasets endpoint.
    /// </summary>
    type GetOptions = {
        datatypeid: string option
        locationids: string list option
        stationids: string list option
        startdate: DateTime option
        enddate: DateTime option
        sortfield: string option
        sortorder: SortOrder option
        limit: int option
        offset: int option
    }

    /// <summary>
    /// Default options for the GetDatasets endpoint. All
    /// fields are set to `None`.
    /// </summary>
    let DefaultGetOptions = {
        datatypeid = None
        locationids = None
        stationids = None
        startdate = None
        enddate = None
        sortfield = None
        sortorder = None
        limit = None
        offset = None
    }

    /// <summary>
    /// Defines functions that target the `"/datasets"` endpoint
    /// of the NOAA API.
    /// </summary>
    type Client (restClient: IRestClient) =
        inherit BaseEndpoint (restClient)

        /// <summary>
        /// Gets information about a single dataset, using the id of the
        /// dataset.
        /// </summary>
        member this.GetDataset (id: string) =
            let request = 
                (new RestRequest("/datasets/{id}", Method.GET))
                    .AddParameter("id", id)

            this.Client.Execute(request)

        /// <summary>
        /// Gets a listing of all of the datasets exposed by the API. Please see
        /// https://www.ncdc.noaa.gov/cdo-web/webservices/v2#datasets for 
        /// documentation.S
        /// </summary>
        member this.GetDatasets (options: GetOptions) =

            let request = (new RestRequest("/datasets", Method.GET))
            
            this.AddQueryParameter request "datatypeid" options.datatypeid
            this.AddQueryParameters request "locationid" options.locationids
            this.AddQueryParameters request "stationid" options.stationids
            this.AddQueryParameter request "sortfield" options.sortfield
            
            this.AddQueryParameter request "startdate" (
                this.ConvertToString options.startdate (fun dt -> dt.ToString("yyyy-mm-dd"))
            )
            
            this.AddQueryParameter request "enddate" (
                this.ConvertToString options.enddate (fun dt -> dt.ToString("yyyy-mm-dd"))
            )

            this.AddQueryParameter request "sortorder" (
                this.ConvertToString options.sortorder EnumerationConversions.sortOrderToString
            )

            this.AddQueryParameter request "limit" (
                this.ConvertToString options.limit Convert.ToString
            )

            this.AddQueryParameter request "offset" (
                this.ConvertToString options.offset Convert.ToString
            )
            
            this.Client.Execute<GetDatasetsResponse>(request)
