namespace NoaaApiClient.V2.Endpoints

open System
open RestSharp

type BaseEndpoint (restClient: IRestClient) =
    member internal this.Client = restClient

    /// <summary>
    /// Adds a query parameter to the specified <see cref="IRestRequest">IRestRequest</see>.
    /// </summary>
    /// <param name="request">The <see cref="IRestRequest">IRestRequest</see> that will receive the parameter.</param>
    /// <param name="parameter">The name of the query string parameter.</param>
    /// <param name="value">The optional string value of the query parameter. If None, the parameter will not be populated.</param>
    member internal this.AddQueryParameter (request: IRestRequest) (parameter: string) (value: string option) =
        match value with
        | None -> ()
        | Some value ->
            ignore <| request.AddQueryParameter (parameter, value)


    /// <summary>
    /// Adds a query parameter to the specified <see cref="IRestRequest">IRestRequest</see>
    /// from a list of string values. Each value will be added to the query string as their 
    /// own parameter. For example, `["value1"; "value2"; ...]` becomes:
    /// `"parameter=value1&parameter=value2&..."`.
    /// </summary>
    /// <param name="request">The <see cref="IRestRequest">IRestRequest</see> that will receive the parameter.</param>
    /// <param name="parameter">The name of the query string parameter.</param>
    /// <param name="value">The optional list of string values for the query parameter. If None, the parameter will not be populated.</param>
    member internal this.AddQueryParameters (request: IRestRequest) (parameter: string) (values: string list option) =
        match values with
        | None -> ()
        | Some values ->
            values |> List.iter
                (fun value -> 
                    this.AddQueryParameter request parameter (Some value)
                )

    /// <summary>
    /// Applies the given function to the value to convert it to a string,
    /// if the value matches `Some value`.
    /// </summary>
    member internal this.ConvertToString (value: 'a option) (conversionFunction: 'a -> string) =
        match value with
        | None -> None
        | Some value -> Some <| conversionFunction value
